export default {
  "touchiconMimeType": "image/png",
  "faviconMimeType": "image/png",
  "precomposed": false,
  "touchicons": [
    {
      "width": 76,
      "src": "/assets/static/src/favicon.png?width=76&key=9502219"
    },
    {
      "width": 152,
      "src": "/assets/static/src/favicon.png?width=152&key=9502219"
    },
    {
      "width": 120,
      "src": "/assets/static/src/favicon.png?width=120&key=9502219"
    },
    {
      "width": 167,
      "src": "/assets/static/src/favicon.png?width=167&key=9502219"
    },
    {
      "width": 180,
      "src": "/assets/static/src/favicon.png?width=180&key=9502219"
    }
  ],
  "favicons": [
    {
      "width": 16,
      "src": "/assets/static/src/favicon.png?width=16&key=309feca"
    },
    {
      "width": 32,
      "src": "/assets/static/src/favicon.png?width=32&key=309feca"
    },
    {
      "width": 96,
      "src": "/assets/static/src/favicon.png?width=96&key=309feca"
    }
  ]
}